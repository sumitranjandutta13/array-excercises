const each = require('./each');
module.exports = function flatten(arr){
    let ret = [];
    for(i=0 ; i< arr.length; i++){
        if(Array.isArray(arr[i])){
            let temp = flatten(arr[i], i, arr);
            each(temp, function(num){
                ret.push(num);
            });
        }else{
            ret.push(arr[i]);
        }
    }
    return ret;
}